import React from 'react';
import { ActivityIndicator } from 'react-native-paper';
import PropTypes from 'prop-types';
import WeatherService from '../services/weather-service'
import { Image, StyleSheet, Text, View, TouchableHighlight} from 'react-native';
import Swipeout from 'react-native-swipeout';
import Loading from '../component/Loading'




class WeatherLine extends React.Component {

  serv = new WeatherService()
  componentDidMount(){
    this.serv.getCityWeather(this.props.element.item.name).then((resp) => {
      this.setState({weather : resp.data})
    })
  }

  render() {
    return (
      this.state.weather != null ? (
      <View style = {this.props.style} key={this.state.weather}>
        <Text>{this.state.weather.name}</Text>
        <Text>{this.state.weather.main.temp}°**</Text>
        <Image style = {{width : 80, height : 80}} source = {{uri : `http://openweathermap.org/img/wn/${this.props.element.item.weather[0].icon}@2x.png`}}/>
      </View> ) :
      (
      <Loading color = "red">
        <Text>Connexion en cours</Text>
    </Loading>
      )
    );
  }
  render(rowData) {
    let swipeBtns = [{
      text: 'Delete',
      backgroundColor: 'red',
      underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
      onPress: () => { this.props.onDelete(this.props.element.item.name) }
    },
    {
      text: 'Favoris',
      backgroundColor: 'blue',
      underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
      onPress: () => { this.props.newHome(this.props.element.item.name) }
    }
  ];

    return (
      <Swipeout right={swipeBtns}
        autoClose='true'
        backgroundColor= 'transparent'>
        <TouchableHighlight
          underlayColor='rgba(192,192,192,1,0.6)'>
          <View>
            <View style = {this.props.style} key={this.props.element.item}>
            <Text>{this.props.element.item.name}</Text>
              <View>
                <Text>{this.props.element.item.main.temp}°</Text>
                <Image style = {{width : 80, height : 80}} source = {{uri : `http://openweathermap.org/img/wn/${this.props.element.item.weather[0].icon}@2x.png`}}/>
              </View>
            </View>
          </View>
        </TouchableHighlight>
      </Swipeout>
    )
  }
}

export default WeatherLine