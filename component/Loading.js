import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import PropTypes from 'prop-types';




class Loading extends React.Component {
  static propTypes = {
    displayColor: PropTypes.string.isRequired // oblige à remplir la propriété 
  };
  render() {
    return (
      <View style={{flex : 1, justifyContent: "center", alignItems : "center"}}>
          <ActivityIndicator size = 'large' color= {this.props.color}/> 
          <Text style = {{color : this.props.color}}>Loading ...</Text>
          {this.props.children}
      </View>
    );
  }
}

export default Loading