import HomePage from '../pages/HomePage'
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import SettingsPage from '../pages/SettingsPage'
import FavoritesPage from '../pages/FavoritesPage'
import AddFavoritesPage from '../pages/AddFavoritesPage'
import { createStackNavigator } from 'react-navigation-stack';



const favoritesNavigator = createStackNavigator(
  {
   Favorites: {
     screen: FavoritesPage
   } ,
  AddFavorites: {
    screen: AddFavoritesPage
  }
  },
  {
    initialRouteName:'Favorites',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: 'lightblue'
      },
      headerTintColor :'#fff',
      headerTitleStyle: {
        fontWeight: 'bold'
      }
    }
  }
)

const tabNavigator = createMaterialBottomTabNavigator(
  
  {
    Home:{ screen : HomePage},
    Settings: {screen : SettingsPage},
    Favorites : {screen : favoritesNavigator}
  },
  {
    initialRouteName : 'Home',
    activeColor:'red',
    inactiveColor : 'white',
    barStyle: { background : 'blue'},
    labelStyle: {textAlign : 'center'}

  }

);
export default tabNavigator;