import axios from "axios"
import React from 'react';

const key = 'f2fdb141df8e2c8c4573fdb7a40ca6ff'
const url = `https://api.openweathermap.org/data/2.5/weather?appid=${key}&units=metric`

class WeatherService{
  findCoordinates() {
    
  };

  getCityWeather(city= 'nanterre'){
    if(!city) return this.getWeatherLatLong()
    return axios.get(`${url}&q=${city.toLowerCase()},fr`)

  } 
  

  getWeatherLatLong(){
    return new Promise(function (resolve, reject) {
      navigator.geolocation.getCurrentPosition(resolve, reject)
    }).then((position) => {
      const location = JSON.stringify(position);
      console.log("link",`${url}&lat=${position.coords.latitude}&lon=${position.coords.longitude}`)
      return axios.get(`${url}&lat=${position.coords.latitude}&lon=${position.coords.longitude}`)
    }).catch((err) => {
      console.error(err.message);
    });

  }

  getWeatherHome(){
    return axios.get(`${url}&q=nanterre,fr`)
  }
}


export default WeatherService;