import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import AppNavigator from './navigation/AppNavigator';
import { Provider } from 'react-redux'
// import { createStore } from 'redux'
import {store} from "./redux/Store"


class App extends React.Component {

  render() {
    return (
      // view, menu, bouton etc ... sont des composants
      <Provider store={store}>
        <AppNavigator/> 
      </Provider>
    ); 
  }

}

export default App;
