import {createStore, combineReducers} from "redux"
import ServicesReducers from "./reducers/ServicesReducers";
import CitiesReducer from "./reducers/CitiesReducer"
const rootReducer = combineReducers({
  serviceReducer: ServicesReducers,
  citiesReducer: CitiesReducer
})
export const store = createStore(rootReducer);
