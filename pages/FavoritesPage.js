import React from 'react';
import { RefreshControl, StyleSheet, Text, View, Button, Alert, AsyncStorage, FlatList, TouchableHighlight } from 'react-native';
import { Input } from 'react-native-elements'

import Modal from "react-native-modal";
import Loading from '../component/Loading'
import WeatherLine from '../component/WeatherLine'
import { Icon } from 'react-native-vector-icons/Ionicons';
import { NavigationEvents } from 'react-navigation';
import WeatherService from '../services/weather-service'
import { connect } from 'react-redux'







// afficher sous forme de liste toutes ces villes et leur température avec l'icone du temps clear
// un titre
// liste avec des ligns
// un bouton + qui ouvre une nouvelle fenêtre pop up
// pop up aura une flêche retour

// Pop up avec titre ajouter, formulaire pour ajouter un favoris


class FavoritesPage extends React.Component {
  serv = new WeatherService();

  state = { isModalVisible: false, citiesName :[], cities : [], refreshing : false };

  onAddPress = () =>{
    console.log("press")
    
  }

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: "Favoris",
      headerRight: (
        <Button title="+" onPress={() => {console.log("compte", params.count); navigation.push('AddFavorites')} } />
        )
    }
  
  }


  componentDidMount(){
  }

  refresh() {
    this.setState({cities : []})
    this.setState({refresh : true})
    AsyncStorage.getItem('cities')
    .then ( (data)=>{ 
      this.setState({citiesName: JSON.parse(data)})
      console.log("cities name", this.state.citiesName)
      this.refreshCities()
    });
  }

  async refreshOneCity(cityName) {
    this.serv.getCityWeather(cityName).then( (response) => {
      console.log("salut")
      let city = response.data
      this.setState({cities : [...this.state.cities, city]})
  

    })
  }
  async refreshCities() {
    this.setState({city : []})
    console.log("coucou")
    for (let cityName in this.state.citiesName) {
      await this.refreshOneCity(this.state.citiesName[cityName].cityName)
  }

}

  deleteCity(cityName){
    console.log("actuellement", this.cityName)
    console.log("tu veux delete", cityName)
    let tab = [...this.state.cities];
    tab.splice(tab.findIndex(e => e == cityName), 1)
    AsyncStorage.setItem('cities', JSON.stringify(tab))
      .then (()=> {
        this.props.navigation.setParams({ count : tab.length})
        this.setState({ cities:tab })
      })    
  }

  newHome(cityName){
    AsyncStorage.setItem('home', cityName)
    console.log("new home", cityName)
  }

  render() {
    return (
      this.state.cities != null ? (
        <View style={{flex : 1}}>
          {/* permet de refresh l'app quand on gobakc */}
        <NavigationEvents onDidFocus={()=> this.refresh()}/>
        
          <FlatList data = {this.state.cities}
            refreshControl = {<RefreshControl refreshing={this.state.refresh} onRefresh={this.refresh} />}
          

            renderItem = {(element)=>  (
              <WeatherLine newHome = {(city) => this.newHome(city)} onDelete = {(city) => this.deleteCity(city)} style={styles.aligned} element = {element}></WeatherLine>
            )} />

  
        </View>) 
        : (
          <Loading color = "red">
               <Text>Connexion en cours ou absence de favoris</Text>
               {/* sinon la page se refresh pas la première fois */}
               <NavigationEvents onDidFocus={()=> this.refresh()}/>
            </Loading>
        )
    );
  }


}


// le css

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  titleBackground:{
    paddingTop: '1%',
    justifyContent:"center",
    alignItems:"center",
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    paddingBottom: '6%',
    backgroundColor: 'lightblue',
  },
  title: {
     flex:1,
     textAlign:"center",
     fontSize: 30
     
  },
  aligned:{
    display:'flex',
    flexDirection: 'row', 
    justifyContent: 'center',
    alignItems:"center",
    justifyContent: "space-around",
    flexWrap:'nowrap'


  },
  main : {
     flex:2,
     justifyContent:"center",
     alignItems:"center",
     padding: '5%',
     backgroundColor: 'lightyellow'

  },
  other : {
     flex:2,
     justifyContent:"center",
     alignItems:"center",
     padding: '5%',
     backgroundColor: 'lightgreen'

  }

});

const mapStateToProps = (stateStore) => ({
  cities: stateStore.citiesReducer.cities
})

export default connect(mapStateToProps)(FavoritesPage);