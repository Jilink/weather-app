import React from 'react';
import WeatherService from '../services/weather-service'
import { Image, StyleSheet, Text, View, Button, Alert, TextInput, AsyncStorage} from 'react-native';
import { Icon } from 'react-native-vector-icons/Ionicons';
import { Input } from 'react-native-elements'


class AddFavoritesPage extends React.Component {
  serv = new WeatherService();

  static navigationOptions = {
    title: 'Ajouter ville'
  }
  state = {
    cityName : ""

 };

 AddCity = () =>{
  this.serv.getCityWeather(this.state.cityName).then( (response) => { // tester si la ville existe dans l'app
    console.log("Data valide")
    this.setState({cityName : response.data.name})
    return AsyncStorage.getItem('cities')
  })
  .then ( (data)=>{ 
    let tab = [];
    if(data !== null){
      tab = JSON.parse(data)
    }
    tab.push(this.state)
    AsyncStorage.setItem('cities', JSON.stringify(tab))
      .then( ()=>{
        console.log("goback")
        this.props.navigation.goBack()
      })
      .catch((err)=> {
        console.log(err)
      })
  })
  .catch( () => { // si la ville n'existe pas
    alert(
      'Sorry but this city does not exist in the database'
    )
    console.log("invalide data")
  })
}

  render() {
    return (
    <View style={{flex : 1}}>
      <Text style={{flex : 1}}>Ajouter une ville</Text>

      <TextInput style={{flex : 1}} onChangeText={(text) => this.setState({cityName : text})} placeholder='Your city'/>
      
      <Button style={{flex : 1}} title="Ajouter" onPress={this.AddCity} />
    </View>
    );}


}

export default AddFavoritesPage;