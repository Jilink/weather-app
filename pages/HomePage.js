import React from 'react';

import { Image, StyleSheet, Text, View, AsyncStorage} from 'react-native';
import Loading from '../component/Loading'
import { NavigationEvents } from 'react-navigation';
import { connect } from 'react-redux'
import {store} from "../redux/Store"






class HomePage extends React.Component {

   state = {
      wea : null,
      location: null
      

   };

   refresh(){
      AsyncStorage.getItem('home').then ( (data)=>{ 
         console.log(data)
         return this.props.weatherServ.getCityWeather(data)
    })
      .then( (response) => {
         console.log(response.data)
         this.setState({wea : response.data})
      })
   }
   

   // attendre que les données soient chargées :
   componentDidMount(){
      console.log("les proprs", this.props)
      this.refresh()
   }
   
   render() {
      // alert(this.state.wea)

      return (
         this.state.wea != null ? ( // est-ce que le render est chargé ? 
            <View style={{flex : 1}}>
               <NavigationEvents onDidFocus={()=> this.refresh()}/>
                  <View style={styles.weather}>
                     <Text style={styles.title}>{this.state.wea.name}</Text>
                     <Text>{this.state.wea.weather[0].main}</Text>
                     <Text>{this.state.wea.weather[0].description}</Text>
                     <Image style = {{width : 80, height : 80}} source = {{uri : `http://openweathermap.org/img/wn/${this.state.wea.weather[0].icon}@2x.png`}}/>


                  </View>
                  
                  

                  <View style={styles.main}>
                     <Text>{this.state.location} Location</Text>
                     <Text>{this.state.wea.main.temp} °</Text>
                     <Text>min : {this.state.wea.main.temp_min} °</Text>
                     <Text>max : {this.state.wea.main.temp_max} °</Text>
                     <Text>humidity : {this.state.wea.main.humidity}</Text>
                     <Text>pressure : {this.state.wea.main.pressure}</Text>

                  </View>

                  <View style={styles.other}>
                     <Text>wind speed : {this.state.wea.wind.speed} km/h</Text>
                     <Sunrise time= {this.state.wea.sys.sunrise}/>
                     <Sunset time= {this.state.wea.sys.sunset}/>

                     {/* <Text>sunrise : {this.state.wea.sys.sunrise} </Text>
                     <Text>sunset : {this.state.wea.sys.sunset} </Text> */}




                  </View>
            </View>
         )
         :
         (
            <Loading color = "red">
               <Text>Connexion en cours</Text>
            </Loading>
            
         )

      );
   }
}

const Sunrise = (props) => {
   const dt = new Date(props.time * 1000);
   return (
      <Text>{`Sunrise at  ${dt.getHours()}:${dt.getMinutes()}:${dt.getSeconds()}`}</Text>
   )
}

const Sunset = (props) => {
   const dt = new Date(props.time * 1000);
   return (
      <Text>{`Sunset at  ${dt.getHours()}:${dt.getMinutes()}:${dt.getSeconds()}`}</Text>
   )
}


// le css

const styles = StyleSheet.create({
   weather: {
      flex:3,
      justifyContent:"center",
      alignItems:"center",
      borderRadius: 4,
      borderWidth: 0.5,
      borderColor: '#d6d7da',
      paddingBottom: '6%',
      backgroundColor: 'lightblue'
   },
   main : {
      flex:2,
      justifyContent:"center",
      alignItems:"center",
      padding: '5%',
      backgroundColor: 'lightyellow'

   },
   other : {
      flex:2,
      justifyContent:"center",
      alignItems:"center",
      padding: '5%',
      backgroundColor: 'lightgreen'

   },
   title : {
      fontSize: 20
   }

 });



const mapStateToProps = (stateStore) => {
   return {
      weatherServ: stateStore.serviceReducer.weatherService
   }
}

export default connect(mapStateToProps)(HomePage)