import React from 'react';
import { StyleSheet, Text, View, Button, AsyncStorage } from 'react-native';


class SettingsPage extends React.Component {
   onDeleteFavoritesPress(){ //delete favoris
      AsyncStorage.removeItem('cities')
   }
   onDeleteHomeFavorite(){ //delete home favorite
      AsyncStorage.removeItem('home')
   }

render() {
  return (
     <View style={{flex : 1, justifyContent: "center", alignItems : "center"}}>
        <Text>Params</Text>
        <Button title = "Delete all favoris" onPress ={() => this.onDeleteFavoritesPress()}/>
        <Button title = "Go back to geolocalisation" onPress ={() => this.onDeleteHomeFavorite()}/>
     </View>
  );
}


}

export default SettingsPage;